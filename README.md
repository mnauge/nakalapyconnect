# NakalaPyConnect
## Dépôt facilitant l'appropriation de Nakala et de son API


*Michael Nauge, Laboratoire FoReLLIS/MIMMOC, Université de Poitiers. Membre du consortium Archive des Ethnologues ; consortium CAHIER ; 2021*


<img src="https://gitlab.huma-num.fr/mnauge/logobib/-/raw/master/HR/bandeauFoReLLIS-MIMMOC-UP.png" alt="bandeau logos FoReLLIS,MIMMOC, UP" width="800"/>


## Contexte et écosystème Nakala
Pour atteindre les objectifs d'ouvertures des *données de la recherche* dans le contexte de la science ouverte, il est important de pouvoir déposer les corpus de données de recherches en SHS dans un service fiable. Le service [Nakala](https://documentation.huma-num.fr/nakala/) supporté par la TGIR Huma-Num semble le parfait candidat.

<img src="./documentation/imgs/ecosystem.svg" alt="schema de l'écosystème Nakala" width="800"/>

*Figure 1 : schéma de l'écosystème Nakala pour la FAIRisation de corpus en SHS. Illustration CC-BY-4.0 par [@R.Nuage](https://rnuage.com/)*

Comme illustré dans la Figure 1. le service Nakala permet :
- La création d'**objets numériques multi-fichiers** accompagnés de métadonnées descriptives. Chaque objet numérique, appelé Data, bénéficie de l'attribution d'un **identifiant pérenne de type DOI**. 

- Les Datas déposées sont également facilements découvrables et indexables grâce au **End-Point OAI-PMH** inclus dans Nakala. Il est également possible de créer des aggrégations de Data en utilisant la notion de Collection Nakala. Chaque Collection pouvant alors fournir un End-Point OAI-PMH facilitant son moissonage par [ISIDORE](https://isidore.science/).

- Pour les adeptes du requetage RDF, les métadonnées des Datas sont égalements interrogeables par un **End-Point Sparql**


- Cerise sur le gateau les fichiers de type images déposées dans une Data sont directement visualisable et manipulable grâce au **End-Point IIIF** inclus dans Nakala. Il n'y a donc plus de question à se poser sur le choix d'un dépôt en tiff et/ou en jpeg avec différents niveaux de résolutions. Il est tout à fait possible de poser des images de très grandes résolutions dans un format sans compression, puis de laisser le server IIIF se charger de faire le transcodage jpg à la volé pour la résoltion d'affichage de son choix.

## Déposer son corpus
Les quelques explications évoquées ci-dessous, suffisent généralement à succiter un fort intérêt car nous mesurons facilement la difficulté de bénéficier de cette qualité de stockage et d'accessibilité de données dans nos établissements.

### Préparation du corpus
Avant de déposer de nombreux fichiers de données et leurs descriptions, il est très important de s'attacher à la **modélisation de ses objets numériques**. Ce document ne traîte pas ce sujet. Nous vous invitons à prendre contact avec l'un des [consortiums disciplinaire Huma-Num](https://www.huma-num.fr/les-consortiums-hn/) ou l'un des [correspondants Huma-Num accessible en MSH](https://www.huma-num.fr/trouver-son-relais-msh/) pour vous accompagner dans cette démarche.


Quelques conseils et points de vigilances au passage sur cette question pour travailler dans de bonnes conditions:

- disposez-vous déjà de quelques métadonnés descriptives (titre, date de création, mots-clé, auteurs...) pour chaque fichier numérique à déposer ? 
> Nakala impose un ensemble minimal de métadonnées à associer à chaque fichier déposé. Les métadonnées : **type, title, creator, created et license** sont obligatoires pour publier une donnée. Complément d'information sur api.nakala.fr/doc en section [datas/post](https://api.nakala.fr/doc#operations-datas-post_datas)

- avez-vous déjà choisi la licence de partage et de réutilisation à attribuer à vos fichiers (Etalab 2.0, CC-BY-NC...) 

- les fichiers physique/numériques disposent t-il d'un système de "plan de classement et de cote archivistique" et d'une convention de nommage ?

- Savez-vous si vos Data Nakala doivent contenir un ou plusieurs fichiers ?


### Premiers pas avec le site web de dépôt
Si vous êtes curieux d'essayer Nakala tout de suite, vous pouvez commencer par "jouer" dans l'espace Nakala bac-à-sable où les données sont régulièrements supprimées pour s'amuser sans risque.

Il suffit de vous rendre sur le site :
[https://test.nakala.fr/](https://test.nakala.fr/)

<img src="./documentation/imgs/nakalaFrontLogin.jpg" alt="front nakala test" width="800"/>

puis de vous identifier avec :
- le login : tnakala
- le pass : IamTesting2020

<img src="./documentation/imgs/nakalaLogin.jpg" alt="front nakala login" width="500"/>

Maintenant identifié, vous pouvez déposer manuellement vos premiers fichiers et leurs métadonnées en utilisant l'interface graphique web dédiée.

### L'interface graphique peut avoir ses limites
Pour de nombreuses raisons, le dépôt *manuel à l'unité* peut sembler peut propice à l'utilisation que vous envisagiées de Nakala.

Voici une liste non exhaustive des raisons qui peuvent vous pousser à expérimenter un autre "mode de dépôt" de données :

- Vous disposez déjà d'une base de données de ressources numériques. Mais cette dernière respect moins les paradigmes FAIR et manque de moyens humains et matériel pour fonctionner. Vous souhaitez donc faire une migration de données sans avoir à tout resaisir à la main.

- Vous disposez de tableurs (csv, xlsx) ou d'arbres (xml, TEI) contenant de nombreuses métadonnées descriptives sur vos ressources. Vous ne souhaitez pas recopier ces informations manuellement au risque d'introduire des erreurs de saisies.

- Vous souhaitez intégrer Nakala au coeur d'une chaine de traitement semi-automatisée de construction de corpus

Pour ce genre de raison, vous allez pouvoir trouver des élèments de réponses en intéragissant avec l'API Nakala à la place de l'interface web graphique.
Créer un système de communication avec une API s'adresse en général à des profils n'ayant pas peur de vouloir parler "machine" en "JSON".



## Expérimenter l'API Nakala

Pour expérimenter et apprivoiser l'API Nakala voici ci-dessous le lien vers un notebook jupyter me permettant d'illustrer de manière modifiable/exécutable une communication avec l'API Nakala. Dans ce notebook, vous allez découvrir les bases de l'API Nakala et son utilisation par un connecteur Python programmé à des fins pédagogiques. L'idée est de vous permettre d'envisager la programmation de votre propre connecteur Nakala dans le language de programmation de votre choix ou d'intégrer ce connecteur python dans votre propre chaine de traitement semi-automatisé.

- Interragir avec le [notebook](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmnauge%2Fnakalapyconnect/HEAD?filepath=notebook%2FplayingWithAPI.ipynb) en mode executable (myBinder). Le lancement peut prendre quelques minutes.

- Consulter le [notebook](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/mnauge/nakalapyconnect/-/raw/master/notebook/playingWithAPI.ipynb) en mode lecture seule (nbviewer). 


